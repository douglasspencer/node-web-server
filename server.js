const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

const PORT = process.env.PORT || 3000;

var app = express();

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');

app.use(express.static(__dirname + '/public'));

app.use((req,res,next) => {
    var now = new Date().toString();
    var logString = `${now} HTTP Method: ${req.method} Path: ${req.url}`
    console.log(logString);
    fs.appendFile('server.log', logString + '\n', (err) => {
        if (err) {
            console.log('Unable to append to server.log');
        }
    });
    
    next();
});


// app.use((req,res,next) => {
//     res.render('maintenace.hbs');
// });



hbs.registerHelper('getCurrentYear',() => {
    return new Date().getFullYear();
});

hbs.registerHelper('ScreamIt',(value) => {
    return value.toUpperCase();
});


app.get('/', (req, res) => {
    res.render('home.hbs', {
        pageTitle: 'Home Page',
        welcomeMessage: 'Welcome To The Home Page'
    });
});

app.get('/about', (req,res) => {
    //res.send('About');
    res.render('about.hbs', {
        pageTitle: 'About Page',
        welcomeMessage: 'Welcome To The About Page'
    });
});

app.get('/bad', (req,res) => {
    res.send({
        message: 'Bad Request Made'
    });
});


app.listen(PORT, () => {
    console.log(`Server Is Running on Port ${PORT}`);
});
